/**
 * @format
 */

import {AppRegistry} from 'react-native';
import Reactions from './Reaction/Reactions';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => Reactions);
