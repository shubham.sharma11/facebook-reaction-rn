import React from 'react';
import styles from './Reactions-styles';
import {Animated, PanResponder, View, Image} from 'react-native';

var images = [
  {id: 'like', img: require('../images/like.gif')},
  {id: 'love', img: require('../images/love.gif')},
  {id: 'angry', img: require('../images/angry.gif')},
  {id: 'haha', img: require('../images/haha.gif')},
  {id: 'wow', img: require('../images/wow.gif')},
];

class Reactions extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selected: '',
      open: false,
    };
    this._imgLayouts = {};
    this._imageAnimations = {};
    this._hoveredImg = '';

    this._scaleAnimation = new Animated.Value(0);

    images.forEach(img => {
      this._imageAnimations[img.id] = {
        position: new Animated.Value(55),
        scale: new Animated.Value(1),
      };
    });
    this._panResponder = PanResponder.create({
      onStartShouldSetPanResponder: (evt, gestureState) => true,
      onStartShouldSetPanResponderCapture: (evt, gestureState) => true,
      onMoveShouldSetPanResponder: (evt, gestureState) => true,
      onMoveShouldSetPanResponderCapture: (evt, gestureState) => true,
      onPanResponderGrant: this.open,
      onPanResponderMove: (evt, gestureState) => {
        var hoveredImg = this.getHoveredImg(
          Math.ceil(evt.nativeEvent.locationX),
        );

        if (hoveredImg && this._hoveredImg !== hoveredImg) {
          this.animateSelected(this._imageAnimations[hoveredImg]);
        }
        if (this._hoveredImg !== hoveredImg && this._hoveredImg) {
          this.animateFromSelect(this._imageAnimations[this._hoveredImg]);
        }

        this._hoveredImg = hoveredImg;
      },
      onPanResponderRelease: (evt, gestureState) => {
        if (this._hoveredImg) {
          this.animateFromSelect(
            this._imageAnimations[this._hoveredImg],
            this.close.bind(this, this.afterClose),
          );
        } else {
          this.close(this.afterClose);
        }
      },
    });
  }

  afterClose = () => {
    if (this._hoveredImg) {
      this.setState({
        selected: this._hoveredImg,
      });
    }

    this._hoveredImg = '';
  };
  animateSelected = imgAnimations => {
    Animated.parallel([
      Animated.timing(imgAnimations.position, {
        duration: 150,
        toValue: -30,
        useNativeDriver: true,
      }),
      Animated.timing(imgAnimations.scale, {
        duration: 150,
        toValue: 1.8,
        useNativeDriver: true,
      }),
    ]).start();
  };
  animateFromSelect = (imgAnimations, cb) => {
    Animated.parallel([
      Animated.timing(imgAnimations.position, {
        duration: 50,
        toValue: 0,
        useNativeDriver: true,
      }),
      Animated.timing(imgAnimations.scale, {
        duration: 50,
        toValue: 1,
        useNativeDriver: true,
      }),
    ]).start(cb);
  };
  getHoveredImg = x => {
    return Object.keys(this._imgLayouts).find(key => {
      return (
        x >= this._imgLayouts[key].left && x <= this._imgLayouts[key].right
      );
    });
  };

  getImageAnimationArray = toValue => {
    return images.map(img => {
      return Animated.timing(this._imageAnimations[img.id].position, {
        duration: 200,
        toValue: toValue,
        useNativeDriver: true,
      });
    });
  };
  open = () => {
    Animated.parallel([
      Animated.timing(this._scaleAnimation, {
        duration: 100,
        toValue: 1,
        useNativeDriver: true,
      }),
      Animated.stagger(50, this.getImageAnimationArray(0)),
    ]).start(() => this.setState({open: true}));
  };
  close = cb => {
    this.setState({open: false}, () => {
      Animated.stagger(100, [
        Animated.parallel(this.getImageAnimationArray(55, 0).reverse()),
        Animated.timing(this._scaleAnimation, {
          duration: 100,
          toValue: 0,
          useNativeDriver: true,
        }),
      ]).start(cb);
    });
  };
  handleLayoutPosition = (img, position) => {
    this._imgLayouts[img] = {
      left: position.nativeEvent.layout.x,
      right: position.nativeEvent.layout.x + position.nativeEvent.layout.width,
    };
  };
  getImages = () => {
    return images.map(img => {
      return (
        <Animated.Image
          onLayout={this.handleLayoutPosition.bind(this, img.id)}
          key={img.id}
          source={img.img}
          style={[
            styles.img,
            {
              transform: [
                {scale: this._imageAnimations[img.id].scale},
                {translateY: this._imageAnimations[img.id].position},
              ],
            },
          ]}
        />
      );
    });
  };
  getLikeContainerStyle = () => {
    return {
      transform: [{scaleY: this._scaleAnimation}],
      overflow: this.state.open ? 'visible' : 'hidden',
    };
  };
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.center} {...this._panResponder.panHandlers}>
          <Image
            style={styles.selectedImg}
            source={
              images.find(img => img.id === this.state.selected)?.img ||
              images[0].img
            }
          />
          <Animated.View
            style={[styles.likeContainer, this.getLikeContainerStyle()]}>
            <View style={styles.borderContainer} />
            <View style={styles.imgContainer}>{this.getImages()}</View>
          </Animated.View>
        </View>
      </View>
    );
  }
}

module.exports = Reactions;
