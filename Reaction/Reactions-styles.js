import {StyleSheet} from 'react-native';
var styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  center: {
    position: 'absolute',
    left: 50,
    top: 300,
  },
  likeContainer: {
    position: 'absolute',
    left: -10,
    top: -40,
    padding: 5,
    flex: 1,
    backgroundColor: '#FFF',
    borderColor: 'transparent',
    borderWidth: 0,
    borderRadius: 20,
  },
  borderContainer: {
    backgroundColor: 'transparent',
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    // borderWidth: 1,
    borderColor: '#444',
    borderRadius: 20,
    marginBottom: 10,
  },
  imgContainer: {
    backgroundColor: 'transparent',
    flexDirection: 'row',
  },
  img: {
    marginLeft: 5,
    marginRight: 5,
    width: 30,
    height: 30,
    overflow: 'visible',
  },
  selectedImg: {width: 25, height: 25},
});
export default styles;
