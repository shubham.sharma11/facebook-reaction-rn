import React from 'react';
import {StyleSheet, Animated, PanResponder, View, Image} from 'react-native';
import createClass from 'create-react-class';
var images = [
  {id: 'like', img: require('./images/like.gif')},
  {id: 'love', img: require('./images/love.gif')},
  {id: 'angry', img: require('./images/angry.gif')},
  {id: 'haha', img: require('./images/haha.gif')},
  {id: 'wow', img: require('./images/wow.gif')},
];

class App extends React.Component {
  state = {
    selected: '',
    open: false,
  };

  afterClose = () => {
    if (this._hoveredImg) {
      this.setState({
        selected: this._hoveredImg,
      });
    }

    this._hoveredImg = '';
  };
  animateSelected = imgAnimations => {
    Animated.parallel([
      Animated.timing(imgAnimations.position, {
        duration: 150,
        toValue: -30,
        useNativeDriver: true,
      }),
      Animated.timing(imgAnimations.scale, {
        duration: 150,
        toValue: 1.8,
        useNativeDriver: true,
      }),
    ]).start();
  };
  animateFromSelect = (imgAnimations, cb) => {
    Animated.parallel([
      Animated.timing(imgAnimations.position, {
        duration: 50,
        toValue: 0,
        useNativeDriver: true,
      }),
      Animated.timing(imgAnimations.scale, {
        duration: 50,
        toValue: 1,
        useNativeDriver: true,
      }),
    ]).start(cb);
  };
  getHoveredImg = x => {
    return Object.keys(this._imgLayouts).find(key => {
      return (
        x >= this._imgLayouts[key].left && x <= this._imgLayouts[key].right
      );
    });
  };

  getImageAnimationArray = toValue => {
    return images.map(img => {
      return Animated.timing(this._imageAnimations[img.id].position, {
        duration: 200,
        toValue: toValue,
        useNativeDriver: true,
      });
    });
  };
  open = () => {
    Animated.parallel([
      Animated.timing(this._scaleAnimation, {
        duration: 100,
        toValue: 1,
        useNativeDriver: true,
      }),
      Animated.stagger(50, this.getImageAnimationArray(0)),
    ]).start(() => this.setState({open: true}));
  };
  close = cb => {
    this.setState({open: false}, () => {
      Animated.stagger(100, [
        Animated.parallel(this.getImageAnimationArray(55, 0).reverse()),
        Animated.timing(this._scaleAnimation, {
          duration: 100,
          toValue: 0,
          useNativeDriver: true,
        }),
      ]).start(cb);
    });
  };
  handleLayoutPosition = (img, position) => {
    this._imgLayouts[img] = {
      left: position.nativeEvent.layout.x,
      right: position.nativeEvent.layout.x + position.nativeEvent.layout.width,
    };
  };
  getImages = () => {
    return images.map(img => {
      return (
        <Animated.Image
          onLayout={this.handleLayoutPosition.bind(this, img.id)}
          key={img.id}
          source={img.img}
          style={[
            styles.img,
            {
              transform: [
                {scale: this._imageAnimations[img.id].scale},
                {translateY: this._imageAnimations[img.id].position},
              ],
            },
          ]}
        />
      );
    });
  };
  getLikeContainerStyle = () => {
    return {
      transform: [{scaleY: this._scaleAnimation}],
      overflow: this.state.open ? 'visible' : 'hidden',
    };
  };
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.center} {...this._panResponder.panHandlers}>
          <Image
            style={{width: 25, height: 25}}
            // onLayout={this.handleLayoutPosition.bind(this, img.id)}
            // key={'1'}
            source={
              images.find(img => img.id === this.state.selected)?.img ||
              images[0].img
            }
          />
          <Animated.View
            style={[styles.likeContainer, this.getLikeContainerStyle()]}>
            <View style={styles.borderContainer} />
            <View style={styles.imgContainer}>{this.getImages()}</View>
          </Animated.View>
        </View>
      </View>
    );
  }
}
var styles = StyleSheet.create({
  container: {
    flex: 1,
    // backgroundColor: 'red'
  },
  center: {
    position: 'absolute',
    left: 50,
    top: 300,
  },
  likeContainer: {
    position: 'absolute',
    left: -10,
    top: -40,
    padding: 5,
    flex: 1,
    backgroundColor: '#FFF',
    borderColor: 'transparent',
    borderWidth: 0,
    borderRadius: 20,
  },
  borderContainer: {
    backgroundColor: 'transparent',
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    // borderWidth: 1,
    borderColor: '#444',
    borderRadius: 20,
    marginBottom: 10,
  },
  imgContainer: {
    backgroundColor: 'transparent',
    flexDirection: 'row',
  },
  img: {
    marginLeft: 5,
    marginRight: 5,
    width: 30,
    height: 30,
    overflow: 'visible',
  },
});
module.exports = App;
